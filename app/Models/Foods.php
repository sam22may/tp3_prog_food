<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Reservation;
class Foods extends Model
{
    use HasFactory;
    public $table = 'foods';

    public function user(){
        return $this->belongsTo(User::class);
    }
}
