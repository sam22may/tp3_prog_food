<?php

namespace App\Http\Controllers;

use App\Models\Foods;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()  // afficher le user dans profil avec les foods lier au user
    {
        $user_id = Auth::user()->id;

        $foods = Foods::where('user_id', $user_id)->get();

        $food_id = Auth::user()->food_id;
        $food = Null;
        if($food_id) {
            $food = Foods::find($food_id);
        }
        return view('profil', compact('food', 'foods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('editprofil', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $request->validate([
            'name' => ['required', 'string', 'max:65'],
            'email' => ['required', 'string', 'email', 'max:65', 'unique:users,email,'.$user->id],
            'address' => ['required', 'string', 'max:65'],
            'city' => ['required', 'string', 'max:65'],
        ]);

        if($request->password){
            $request->validate(([
                'password' => ['required', 'confirmed', Rules\Password::defaults()],
            ]));
            $user->password = Hash::make($request->password);
        }

        $path = $user->image;
        if($request->image){
            $path = basename($request->image->store('images', 'public'));
            // Enregistre l'image réduite dans le dossier '/storage/app/public/thumbs'
            $image = InterventionImage::make($request->image)->widen(300)->encode();
            Storage::put('public/thumbs/' . $path, $image);
        }
        
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->image = $path;
        $user->food_id = Null;
       

        $user->save();

        return redirect('/profil')->with('status', "Le profil a bien été mis à jour.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->image !== 'profil_vide.jpg'){
            $path = $user->image;
            storage::delete(['public/thumbs/' . $path, 'public/images/' . $path]);
        }
        $foods = Foods::where('user_id', $id)->get();

        foreach($foods as $food){
            $path = $food->image;
            storage::delete(['public/thumbs/' . $path, 'public/images/' . $path]);
            Foods::destroy($food->id);
        }
       
        User::destroy($id);

        return redirect('/')->with('status', 'Profil supprimé.');
    }
}
