<?php

namespace App\Http\Controllers;

use App\Image;
use App\Models\Foods;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $foods = Foods::orderBy('created_at', 'desc')->paginate(9); // va chercher toutes les foods, les filtre par ordres du plus récent au plus vieux et pagine par 9 éléments
        return view('welcome', compact('foods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addFood');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|max:255',
            'image' => 'required',
        ]);

        $path = "";
        if($request->image){
            $path = basename($request->image->store('images', 'public'));
        }

        // Enregistre l'image réduite dans le dossier '/storage/app/public/thumbs'
        $image = InterventionImage::make($request->image)->widen(300)->encode();
        Storage::put('public/thumbs/' . $path, $image);

        $foods = new Foods;
        $foods->description = $request->input('description');
        $foods->image = $path;
        $foods->meteo = $request->input('meteo');
        $foods->user_id = $request->input('user_id');
        $foods->is_reserved = 0;
        $foods->save();

        return redirect('/')->with("status", "Le don est ajouté dans la communauté!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $food = Foods::find($id);
        return view('addFood', compact('food'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $foods = Foods::find($id);

        $request->validate([
            'description' => 'required|max:255',
        ]);

        $path = $foods->image;

        if($request->image) {
            $path = basename($request->image->store('images', 'public'));
            // Enregistre l'image réduite dans le dossier '/storage/app/public/thumbs'
            $image = InterventionImage::make($request->image)->widen(300)->encode();
            Storage::put('public/thumbs/' . $path, $image);
        }

        $foods->description = $request->input('description');
        $foods->image = $path;
        $foods->meteo = $request->input('meteo');
        $foods->user_id = $request->input('user_id');
        $foods->is_reserved = 0;
        $foods->save();

        return redirect('/')->with('status', 'Les modifications ont été sauvegardé.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function destroy($id) // suppression de la food par le user connecté dans la page accueil
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        $user->food_id = null;
        $user->save();

        $food = Foods::find($id);
        $path = $food->image;
        storage::delete(['public/thumbs/' . $path, 'public/images/' . $path]);
        $delete = Foods::destroy($id);

        if ($delete) {
            return response()->json([
                'id' => $id
            ], 200);
        } else {
            return response()->json([
                'message' => 'Il y a eue un problème'
            ], 404);
        }
    }

    public function destroyfood($id) // function destroy 2 pour lorsque la food a été récupérer dans le profil
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        $user->food_id = null;
        $user->save();

        $food = Foods::find($id);
        $path = $food->image;
        storage::delete(['public/thumbs/' . $path, 'public/images/' . $path]);
        Foods::destroy($id);

        return redirect('/profil')->with('status', 'Action effectué avec succès.');
    }
}
