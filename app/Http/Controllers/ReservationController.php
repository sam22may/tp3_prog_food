<?php

namespace App\Http\Controllers;

use App\Models\Foods;
use App\Models\User;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id, $food_id)
    public function update(Request $request, $food_id, $user_id) // function pour reservé la food de 0 a 1 (false a true) 
    {
        $user = User::find($user_id);
        if(!$user->food_id) {
            $user->food_id = $food_id;
            $food = Foods::find($food_id);
            $food->is_reserved = 1;
            $user->save();
            $food->save();
        }

        return redirect('/')->with('status', 'Nourriture réservé, allez dans votre profil pour la récupérer!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $food_id, $user_id) // function pour annuler la réservation de 1 a 0 (true a false) 
    {
        $user = User::find($user_id);
        $user->food_id = NULL;
        $food = Foods::find($food_id);
        $food->is_reserved = 0;
        $user->save();
        $food->save();

        return redirect('/');
    }
}
