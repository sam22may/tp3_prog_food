<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Intervention\Image\Facades\Image as InterventionImage;
use Illuminate\Support\Facades\Storage;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([ // valider la requete
            'name' => ['required', 'string', 'max:65'],
            'email' => ['required', 'string', 'email', 'max:65', 'unique:users'],
            'address' => ['required', 'string', 'max:65'],
            'city' => ['required', 'string', 'max:65'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $path = "profil_vide.jpg";
        if($request->image){ // si la request dans image est vrai
            $path = basename($request->image->store('images', 'public'));
            // Enregistre l'image réduite dans le dossier '/storage/app/public/thumbs'
            $image = InterventionImage::make($request->image)->widen(300)->encode();
            Storage::put('public/thumbs/' . $path, $image);
        }

        $user = User::create([ //céer un nouveau user avec les informations
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'city' => $request->city,
            'image' => $path,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME)->with('status', 'Votre profil a été créé. Bienvenue dans la communauté!');
    }
}
