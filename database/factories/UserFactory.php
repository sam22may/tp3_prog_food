<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $city = ["Quebec", "Montreal", "Sherbrooke", "Trois-Rivieres", "Chicoutimi", "Laval", "Gatineau", "Drummondville"];

        static $number1 = 1;
        static $number2 = 1;

        return [
            'name' => $this->faker->name(),
            'email' => "user".$number1++."@user".$number2++.".ca", // incrémente les nombre pour faire user1@user1.com user2@user2.com, etc.
            'email_verified_at' => now(),
            'address' => $this->faker->streetAddress(),
            'city' => $city[rand(0, 7)],
            'image' => 'profil_vide.jpg',
            'password' => Hash::make('123456'), // password
            'remember_token' => Str::random(10),
        ];
    }


    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
