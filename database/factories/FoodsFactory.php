<?php

namespace Database\Factories;

use App\Models\Foods;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User;

class FoodsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Foods::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user_id = rand(1, 5);
        $user = User::find($user_id);
        $meteo = $user->city;
        return [
            'description' => $this->faker->text($maxNbChars = 255),
            'image' => $this->faker->imageUrl($width = 640, $height = 480, 'cats', true, 'Faker'),
            'meteo' => $meteo,
            'is_reserved' => 0,
            'user_id' => $user_id,
        ];
    }
}
