<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// page accueil
Route::get('/', [FoodController::class, 'index'])->name('welcome');
// profil est la vue après le Login
Route::get('/profil', [UserController::class, 'index'])->middleware(['auth'])->name('profil');
Route::get('/profil/edit/{id}', [UserController::class, 'edit'])->middleware(['auth'])->name('edit_profil');
Route::post('/profil/update/{id}', [UserController::class, 'update'])->middleware(['auth'])->name('update_profil');;
//delete profil
Route::delete('/profil/delete_profil/{id}', [UserController::class, 'destroy'])->name('delete_profil');

// get et post food
Route::get('/addfood', [FoodController::class, 'create'])->middleware(['auth'])->name('addfood');
Route::post('/addfood', [FoodController::class, 'store'])->middleware(['auth']);

// get et update food
Route::get('/editfood/{id}', [FoodController::class, 'edit'])->middleware(['auth'])->name('editfood');
Route::post('/editfood/{id}', [FoodController::class, 'update'])->middleware(['auth']);

// delete food (accueil et profil)
Route::delete('/deletefood/{id}', [FoodController::class, 'destroy']);
Route::delete('/profil/destroyfood/{id}', [FoodController::class, 'destroyfood']);

// reservé et annulé la food
Route::post('/reserve/{id}/{user_id}', [ReservationController::class, 'update']);
Route::post('/reserved/{id}/{user_id}', [ReservationController::class, 'destroy']);




require __DIR__.'/auth.php';
