<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Bienvenue {{ Auth::user()->name }}, tu es connecté !
        </h2>
    </x-slot>

    @if(session('status'))
    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-blue-200 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-blue-200 border-b border-gray-200">
                    {{ session('status') }}

                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="flex justify-center mb-5">
        <p id="status" class="p-6 bg-blue-200 rounded text-center rounded-lg fixed-msg">Nourriture supprimé avec succès.</p>
    </div>

    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <p class="text-xl mb-4 text-blue-700">Nourriture réservé</p>

                    @if($food)
                    <div class="mb-8 mt-4">
                        <p class="py-4">Cliquez lorsque la nourriture est récupéré -></p>
                        <form action="{{ url('/profil/destroyfood/'.$food->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="text-lg bg-blue-700 hover:bg-blue-400 text-white p-2 rounded-xl">Nourriture récupéré</button>
                        </form>
                    </div>

                    @include ('inc.carte')
                    @else
                    <a href="/" class="underline hover:text-blue-500">Parcourir les dons disponible</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="pb-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <p class="text-xl mb-4 text-blue-700">Don en attente</p>

                    @forelse ($foods as $userfood)
                    <div class="p-2 bg-blue-100 rounded-lg m-2 inner-shadow">
                        <p># Identification : {{ $userfood->id }} </p>
                        <div class="hidden">{{ $userfood->id }}</div>
                        <p>{{ $userfood->description }}</p>
                        <div class="flex">
                            <div class="mb-1 mr-3">
                                <a href="{{ route('editfood', $userfood->id) }}" class="mr-3 bg-green-400 hover:bg-blue-700 text-white font-bold p-1 rounded-xl">Modifier</a>
                                <button id="deleteBtnProfil{{ $userfood->id }}" btnid="{{ $userfood->id }}" type="button" class="deleteBtnProfil bg-red-500 hover:bg-blue-700 text-white p-1 rounded-xl">Supprimer</button>
                            </div>
                            <div id="confirmation{{ $userfood->id }}" class="confirmation-delete p-3 bg-blue-200 mb-1">
                                <p>Supprimer le don #{{ $userfood->id }} ?</p>
                                <form action="{{ url('/profil/destroyfood/'.$userfood->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="bg-red-500 hover:bg-blue-700 text-white p-2 rounded-xl">Confirmer</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @empty
                    <a href="/addfood" class="underline hover:text-blue-500">Ajouter un don</a>
                    @endforelse
                </div>
            </div>
        </div>
    </div>

    <div class="pb-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex justify-between">
                        <h3 class="text-xl text-blue-700">Informations personnelles</h3>
                        <a href="{{ route('edit_profil', [Auth::user()->id]) }}" class="text-xl bg-green-100 hover:bg-green-400 text-green-700 p-2 hover:text-white rounded">Modifier</a>
                    </div>

                    <p>Nom : {{ Auth::user()->name }}</p>
                    <p>Courriel : {{ Auth::user()->email }}</p>
                    <p>Adresse : {{ Auth::user()->address }}</p>
                    <p>Ville : {{ Auth::user()->city }}</p>
                    <p>Image de profil : </p>
                    @if (Auth::user()->image === "profil_vide.jpg")
                    <img src="/images/{{ Auth::user()->image }}" alt="profil">
                    @else
                    <img src="/storage/thumbs/{{ Auth::user()->image }}" alt="profil">
                    @endif

                    <button id="deleteProfil" class="bg-red-500 text-white p-2 mt-2" type="button">Supprimer mon profil</button>
                    <form action="{{ route('delete_profil', [Auth::user()->id]) }}" method="POST" id="confirmationDeleteProfil" class="hidden bg-blue-200 p-3">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <p class="p-1">Voulez-vous supprimer votre profil ?</p>
                        <button type="submit" class="p-1 bg-red-500 text-white hover:bg-red-400 rounded-xl">Confirmer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>