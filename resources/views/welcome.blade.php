@include('inc.header')
<?php
\Carbon\Carbon::setLocale('fr');
setlocale(LC_TIME, 'French');
?>

<main class="allcard-container mt-40">
    @if(session('status'))
    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-blue-200 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-blue-200 border-b border-gray-200">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="flex justify-center mb-5">
        <p id="status" class="p-6 bg-blue-200 rounded text-center rounded-lg fixed-msg">Nourriture supprimé avec succès.</p>
    </div>
    <section class="gap-10 ml-5 justify-center grid grid-cols-1 lg:grid-cols-1 xl:grid-cols-2 2xl:grid-cols-3 ">
        @if(isset($message))
        {{ $message }}
        @endif
        @forelse ($foods as $food)
        @if (!$food->is_reserved)
        @include('inc.carte')
        @endif
        @empty
        <h2 class="text-xl">Aucune nourriture est présentement disponible. Connecte toi pour ajouter un don pour la communauté!</h2>
        @endforelse
    </section>

    <div class="mt-5 p-2 bg-blue-op rounded">
        {{ $foods->links() }}
    </div>

</main>
@include('inc.footer')
