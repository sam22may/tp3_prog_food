<?php

use Illuminate\Support\Facades\Http;

$image = $food->image;
$user = $food->user;
$city = $user->city;

$apiKey = "3a08e8c03d52ad705475d06cd33a527c";
$api_data = Http::get("api.openweathermap.org/data/2.5/weather?q=" . $city . "&units=metric&appid=" . $apiKey)->json();

$temp = round($api_data['main']['temp']);
?>

<div id="food-{{ $food->id }}" class="card-container bg-gradient-to-b from-green-100 to-blue-500 rounded box-shadow flex flex-col justify-between justify-self-center">
    <div class="flex justify-center rounded">
        <img src=@if (Str::startsWith($image, 'https:' )) "{{ $image }}" @else "/storage/thumbs/{{ $image }}" @endif alt="nourriture" class="img rounded">
    </div>
    <div class="p-5">
        <div class="flex items-center space-between py-2">
            <?php
            $path = 'reserve/';
            $reservation = "Cliquez pour reservé";

            if ($food->is_reserved) {
                $path = 'reserved/';
                $reservation = 'Annuler la réservation';
            }
            ?>
            @if ($food->is_reserved === 0)
            <p class="text-sm amber rounded-xl p-1 mr-2">Disponible {{ $food->created_at->diffForHumans() }}</p>
            @auth

            <form action="{{ url($path . $food->id . '/' . Auth::user()->id ) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <button type="submit" name="food_id" class="bg-red-500 hover:bg-blue-700 text-white font-bold p-1 rounded-xl"><?= $reservation ?></button>
            </form>
            @endauth
            @else
            <p class="text-sm amber rounded-xl p-1 mr-2">Disponible {{ $food->created_at->diffForHumans() }}</p>
            @auth

            <form action="{{ url($path . $food->id . '/' . Auth::user()->id ) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <button type="submit" name="food_id" class="bg-red-500 hover:bg-blue-700 text-white font-bold p-1 rounded-xl"><?= $reservation ?></button>
            </form>
            @endauth
            @endif
        </div>
        <div class="flex flex-col">
            <p class="text-xl font-bold "> {{ $food->meteo }} {{$temp}}°</p>
            <p>{{ $food->description }}</p>
        </div>
    </div>

    <div class="bg-blue-500 flex items-center">
        <div class="p-2">
            <div>
                <img src=@if ($food->user->image === "profil_vide.jpg")
                "/images/profil_vide.jpg"
                @else
                "/storage/thumbs/{{ $food->user->image }}"
                @endif
                alt="profil picture" class="rounded-img">
            </div>
        </div>
        <div class="mr-2">
            @if($food->user_id != null)
            <p> {{ $food->user->name }} </p>
            @endif
        </div>
        @auth
        @if( Auth::user()->id === $food->user_id )
        <div class="mr-2">
            <a href="{{ route('editfood', $food->id) }}" class="bg-green-400 hover:bg-blue-700 text-white font-bold p-1 rounded-xl inline-block">Modifier</a>
        </div>
        <div>
            <button type="button" btnid="{{ $food->id }}" class="deleteBtnCard bg-red-500 hover:bg-blue-700 text-white font-bold p-1 rounded-xl">Supprimer</button>
        </div>


        @endif
        @endauth


    </div>
    <div id="confirmation{{ $food->id }}" class="hidden p-2 bg-blue-400 flex justify-end">
        <p class="mr-2">Supprimer le don ?</p>
        @include('inc.boutonDelete')
    </div>

</div>
