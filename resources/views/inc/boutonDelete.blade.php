<form action="{{ url('/deletefood/'.$food->id) }}" method="POST" class="form-delete" >
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit" class="bg-red-500 hover:bg-blue-700 text-white font-bold p-1 rounded-xl">Supprimer</button>
</form>
