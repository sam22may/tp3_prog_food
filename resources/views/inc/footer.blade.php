        </div>

        <footer class="flex justify-between py-6 px-48 bg-gray-50 shadow-inner">
            <div>
                <a href="/"><h1 class="text-xl underline hover:text-blue-700">Nourriture Communautaire</h1></a>
                <p>Tous droits réservés | 2021</p>
            </div>
            <div>
                <p>TP3 - Programmation 2</p>
                <p>Nicolas Tremblay et Samuel Le May</p>
            </div>
            
        </footer>
        
        <script src="{{ URL::asset('js/app.js') }}"></script>
    </body>
</html>