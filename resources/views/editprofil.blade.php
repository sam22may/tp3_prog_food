<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight mr-4">
            Modification du profil {{ Auth::user()->name }}
        </h2>
    </x-slot>

    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('update_profil', [$user->id]) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="flex justify-between">
                            <h3 class="text-xl">Informations personnelles</h3>
                            <div>
                                <a href="/profil" class="p-2 mr-4 rounded bg-red-100 hover:bg-red-200">Annuler</a>
                                <button type="submit" class="text-xl bg-green-100 hover:bg-green-400 text-green-700 p-2 hover:text-white rounded">Sauvergarder</button>
                            </div>

                        </div>
                        @if ($errors->any())
                        @foreach ($errors->all() as $error)
                        <li class="text-red-500">{{ $error }}</li>
                        @endforeach
                        @endif

                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <!-- Nom -->
                        <label for="name" class="block">Nom</label>
                        <input type="text" name="name" value="{{ $user->name }}" class="block mb-4">
                        <!-- Courriel -->
                        <label for="email" class="block">Courriel</label>
                        <input type="text" name="email" value="{{ $user->email }}" class="block mb-4">
                        <!-- Adresse -->
                        <label for="address" class="block">Adresse</label>
                        <input type="text" name="address" value="{{ $user->address }}" class="block mb-4">
                        <!-- Ville -->
                        <label for="city" class="block">Ville</label>
                        <input type="text" name="city" value="{{ $user->city }}" class="block mb-4">

                        <!-- changer mot de passe, appuyer sur le bouton pour faire apparaitre les champs -->
                        <button id="pwchangebtn" type="button" class="block mb-4 p-2 bg-blue-300 hover:bg-blue-400">Changer le mot de passe</button>
                        <div id="pw-hidden" class="hidden">
                            <!-- Password -->
                            <p>Entrer un nouveau mot de passe ou laisser les champs vides pour garder le mot de passe actuel</p>
                            <label for="password">Mot de passe</label>
                            <input id="password" class="block mb-4" type="password" autocomplete="new-password" name="password" placeholder="********"/>
                            <!-- Confirm Password -->
                            <label for="password_confirmation">Confirmé mot de passe</label>
                            <input id="password_confirmation" class="block mb-4" autocomplete="new-password" type="password" name="password_confirmation" placeholder="********"/>
                        </div>
                        <!-- Image -->
                        <label for="image">Image de profil</label>
                        <input id="image" class="block mb-4" type="file" accept="image/png, image/jpg" name="image" />

                    </form>
                    

                    @if (Auth::user()->image === "profil_vide.jpg")
                    <img src="/images/{{ Auth::user()->image }}" alt="profil">
                    @else
                    <img src="/storage/thumbs/{{ Auth::user()->image }}" alt="profil">
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>