@include('inc.header')



@if(isset($food))
<?php
$image = $food->image
?>
<!-- UPDATE UN DON  -->
<main class="allcard-container mt-40">
    @if(session('status'))
    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-blue-200 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-blue-200 border-b border-gray-200">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="bg-blue-op rounded p-10 border-2 border-blue-500">
        <h2 class="text-3xl mb-4">Modifier un don de nourriture</h2>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <li class="text-red-500">{{ $error }}</li>
        @endforeach
        @endif
        <form action="" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="user_id" value="{{ Auth::user()->id }}" hidden>

            <label for="description">Description détaillé de la nourriture ainsi que l'endroit où elle sera déposé.</label>
            <input type="text" name="description" value="{{ $food->description }}" class="block mb-4">
            <br>

            <img src=@if (Str::startsWith($image, 'https:' )) "{{ $image }}" @else "/storage/thumbs/{{ $image }}" @endif alt="placeholder" class="img rounded">
            <label for="image">Pour changer l'image, faite une nouvelle sélection :</label>
            <input type="file" id="image" name="image" accept="image/png, image/jpg" class="block mb-4 input-desc">

            <input name="meteo" value="{{ $food->meteo }}" hidden>

            <button type="submit" class="p-2 rounded bg-green-300 hover:bg-green-400">Sauvegarder</button>
        </form>
    </div>
</main>
@else
<!-- AJOUTER UN DON -->
<main class="allcard-container mt-40">
    @if(session('status'))
    <div class="py-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-blue-200 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-blue-200 border-b border-gray-200">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="bg-blue-op rounded p-10 border-2 border-blue-500">
        <h2 class="text-3xl mb-4">Ajouter un don de nourriture</h2>
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <li class="text-red-500">{{ $error }}</li>
        @endforeach
        @endif
        <form action="/addfood" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="user_id" value="{{ Auth::user()->id }}" hidden>

            <label for="description">Description détaillé de la nourriture ainsi que l'endroit où elle sera déposé.</label>
            <input type="text" name="description" class="block mb-4 input-desc">

            <label for="image">Fournir une image de la nourriture à donner.</label>
            <input type="file" id="image" name="image" accept="image/png, image/jpg" class="block mb-4">

            <!-- <label for="meteo">Météo</label> -->
            <input name="meteo" value="{{ Auth::user()->city }}" hidden>

            <button type="submit" class="p-2 rounded bg-green-300 hover:bg-green-400">Soumettre</button>
        </form>
    </div>
</main>
@endif


@include('inc.footer')