@include ('inc.header')
<main class="allcard-container mt-40">

    <h1>Page de Profil de {{ Auth::user()->name }}</h1>
    User info disponible : {{ Auth::user() }}

    @if($food)
        {{ $food }}
    @include ('inc.carte')
    @endif

</main>
@include ('inc.footer')


