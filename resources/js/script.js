'use strict';

let token = document.querySelector('meta[name=csrf-token]').getAttribute('content')
let select_delete = document.getElementsByClassName('form-delete')

Array.from(select_delete).forEach(select_item => {
    select_item.addEventListener('click', function(event) {
        event.preventDefault()
        let href = event.target.form.action
        onClick(href)
    })
});

function alertTimeOut() {
    setTimeout(function(){ document.getElementById('status').style.visibility = "hidden"; }, 5000);
  }


function onClick(href) {
    fetch(href, {
        headers: {
           "X-CSRF-TOKEN": token
        },
        method: 'DELETE',
    })
    .then((response) => {
        return response.json()
    })
    .then((data) => {
        document.getElementById('status').style.visibility = "visible";
        alertTimeOut()
        document.getElementById('food-' + data.id).remove()
    })
    .catch(error => {
        console.log('error', error)
    })
}




// confirmation food delete dans profil
let deleteBtnProfil = document.getElementsByClassName('deleteBtnProfil')

Array.from(deleteBtnProfil).forEach(delete_btn => {
    delete_btn.addEventListener('click', function(e) {
        let btnId = e.target.getAttribute('btnid')
        let confirmation = document.getElementById(`confirmation${btnId}`)
        if(confirmation.style.display !== "block") {
            e.target.textContent = "Annuler"
            confirmation.style.display = "block"
        } else {
            e.target.textContent = "Supprimer"
            confirmation.style.display = "none"
        }
    })
});

// confirmation food delete dans carte
let deleteBtnCard = document.getElementsByClassName('deleteBtnCard')

Array.from(deleteBtnCard).forEach(delete_btn_card => {
    delete_btn_card.addEventListener('click', function(e) {
        let btnId = e.target.getAttribute('btnid')
        let confirmation = document.getElementById(`confirmation${btnId}`)
        if(confirmation.style.display !== "flex") {
            e.target.textContent = "Annuler"
            confirmation.style.display = "flex"
        } else {
            e.target.textContent = "Supprimer"
            confirmation.style.display = "none"
        }
    })
});

// confirmation profil delete 
let deleteProfil = document.getElementById('deleteProfil')

if (deleteProfil !== null){
    deleteProfil.addEventListener('click', function(e) {
        let confirmation = document.getElementById('confirmationDeleteProfil')
        if(confirmation.style.display !== "block") {
            e.target.textContent = "Annuler"
            confirmation.style.display = "block"
        } else {
            e.target.textContent = "Supprimer mon profil"
            confirmation.style.display = "none"
        }
    })
}


// bouton faire apparaitre les champs mot de passe
let pwChangeBtn = document.getElementById('pwchangebtn')
let pwHidden = document.getElementById('pw-hidden')

if (pwChangeBtn !== null){
    pwChangeBtn.addEventListener('click', function(){
        if(pwHidden.style.display === "none"){
            pwHidden.style.display = "block"
        } else {
            pwHidden.style.display = "none"
        }
    })
}

